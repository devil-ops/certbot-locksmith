# Certbot Locksmith

Utilities to interact with the Certbot and Locksmith implementation of ACME.

## Installation

Download binaries from the [release page](https://gitlab.oit.duke.edu/devil-ops/certbot-locksmith/-/releases)

You can also pull from the normal devil-ops locations...check out the
[installing-devil-ops-packages](https://gitlab.oit.duke.edu/devil-ops/installing-devil-ops-packages)
for more info

## Use dns-01 challenge

These scripts are based off of the documentation
[here](https://eff-certbot.readthedocs.io/en/stable/using.html#pre-and-post-validation-hooks)

You can use the following hooks to interact with the Certbot and Locksmith
implementation of ACME. Note that you may need to alter the command below to fit
your environment.

```bash
$ export AKA_USER=<your-username>
$ export AKA_KEY=<your-api-key>
$ certbot certonly --manual \
  --server https://locksmith.oit.duke.edu/acme/v2/directory --preferred-challenges=dns \
  --manual-auth-hook /usr/bin/certbot-locksmith-auth-hook \
  --manual-cleanup-hook /usr/bin/certbot-locksmith-cleanup-hook \
  --email YOUR_EMAIL@duke.edu --agree-tos --no-eff-email \
  -d secure.example.com
...
```

### Troubleshooting

Set `CERTBOT_DEBUG` to a non empty value to get more information about the process.

### Security Considerations

In general, using the `http-01` challenge is recommended. `http-01` does not
require any additional credentials, which is one less thing to be misused if
leaked.

## Use the http-01 challenge

Certbot works with the http-01 challenge challenge without the need for
additonal software hooks. Please see the locksmith
[documentation](https://locksmith.oit.duke.edu/help/acme) for information on how
to use this.

*This code is freely available for non-commercial use and is provided as-is with no warranty.*

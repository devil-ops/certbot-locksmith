package main

import (
	"context"
	"os"
	"strings"

	"github.com/apex/log"
	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
	"gitlab.oit.duke.edu/devil-ops/certbot-locksmith/certbot"
)

func main() {
	debug := os.Getenv("CERTBOT_DEBUG")
	if debug != "" {
		log.SetLevel(log.DebugLevel)
	}

	err := certbot.ValidateCleanupEnv()
	certbot.CheckErr(err, "")

	certbotDomain := os.Getenv("CERTBOT_DOMAIN")

	// Create a new aka API client
	akaClient, err := aka.NewClientWithEnv()
	certbot.CheckErr(err, "Could not create AKA client")

	// Even though we are only using CERTBOT_DOMAIN now, do a loop for when
	// we use CERTBOT_ALL_DOMAINS maybe?
	ctx := context.Background()
	domains := []string{certbotDomain}
	for _, domain := range domains {
		deleteDomain := certbot.CreateChallengeDomainFromDomain(domain)
		// Trying to be extra careful here to as never delete the actual domain from DNS
		domain = "NEVER_USE_AGAIN"
		log.WithFields(log.Fields{
			"domain": domain,
		}).Debug("Bustin domain var just in case it is used in the future")

		// Be very liberal with the fail here. In theory we never get to this, but using as a failsafe🤞
		if !strings.HasPrefix(deleteDomain, "_acme-challenge.") {
			log.WithFields(log.Fields{
				"deleteDomain": deleteDomain,
			}).Fatal("Domain must start with _acme-challenge. Something is way messed up, quitting out of abundance of caution")
		}
		// Does the domain exist with a TXT record?
		var foundTXT bool
		res, err := akaClient.GetDNSRecord(ctx, deleteDomain)
		certbot.CheckErr(err, "Could not check if DNS record exists")
		for _, item := range res {
			if item.Type == "TXT" {
				foundTXT = true
				break
			}
		}
		if !foundTXT {
			log.WithFields(log.Fields{
				"deleteDomain": deleteDomain,
			}).Debug("Domain not found, yay?")
			continue
		}
		for _, record := range res {
			// Only touch TXT records to be extra safe
			if record.Type != "TXT" {
				log.WithFields(log.Fields{
					"deleteDomain": deleteDomain,
					"record":       record,
				}).Info("Leaving non-TXT record alone")
				continue
			}
			log.WithFields(log.Fields{
				"path":         record.EditPath,
				"deleteDomain": deleteDomain,
			}).Debug("About to delete domain")
			_, err := akaClient.DeleteDNSRecord(ctx, record.EditPath)
			if err != nil {
				log.WithFields(log.Fields{
					"deleteDomain": deleteDomain,
					"record":       record.EditPath,
					"error":        err,
				}).Fatal("Could not delete DNS record")
			}
		}
		log.WithFields(log.Fields{
			"deleteDomain": deleteDomain,
		}).Debug("Cleaned up domain!")
	}
}

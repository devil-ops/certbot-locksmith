package certbot

import (
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/apex/log"
)

func ExtractDomain(fqdn string) (string, error) {
	fqdn = strings.TrimSuffix(strings.TrimSpace(fqdn), ".")
	pos := strings.Index(fqdn, ".")
	if pos == -1 {
		return "", errors.New("Could not extract domain from FQDN. Missing '.'")
	}
	return fqdn[pos+1:], nil
}

func NormalizeDomain(domain string) string {
	domain = strings.TrimSpace(domain)
	domain = strings.ToLower(domain)
	domain = strings.TrimSuffix(domain, ".")
	return domain
}

func CreateChallengeDomainFromDomain(domain string) string {
	return fmt.Sprintf("_acme-challenge.%v", domain)
}

func ValidateAuthEnv() error {
	if os.Getenv("CERTBOT_VALIDATION") == "" {
		return errors.New("CERTBOT_VALIDATION must be set. Are you running this hook from Certbot?")
	}
	if os.Getenv("CERTBOT_DOMAIN") == "" {
		return errors.New("CERTBOT_DOMAIN must be set. Are you running this hook from Certbot?")
	}
	return nil
}

func ValidateCleanupEnv() error {
	if os.Getenv("CERTBOT_DOMAIN") == "" {
		return errors.New("CERTBOT_DOMAIN must be set. Are you running this hook from Certbot?")
	}
	return nil
}

func CheckErr(err error, msg string) {
	if msg == "" {
		msg = "Fatal Error occurred"
	}
	if err != nil {
		log.WithError(err).Fatal(msg)
	}
}

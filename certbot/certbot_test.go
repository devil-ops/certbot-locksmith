package certbot

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCreateDomainFromDomain(t *testing.T) {
	tests := []struct {
		domain string
		want   string
	}{
		{
			domain: "example.com",
			want:   "_acme-challenge.example.com",
		},
	}
	for _, tt := range tests {
		got := CreateChallengeDomainFromDomain(tt.domain)
		require.Equal(t, tt.want, got)
	}
}

func TestExtractDomain(t *testing.T) {
	tests := []struct {
		fqdn    string
		want    string
		wantErr bool
	}{
		{
			fqdn: "example.com",
			want: "com",
		},
		{
			fqdn: "foo.example.com.",
			want: "example.com",
		},
		{
			fqdn: "foo.example.com ",
			want: "example.com",
		},
		{
			fqdn:    "example",
			want:    "",
			wantErr: true,
		},
	}

	for _, tt := range tests {
		got, err := ExtractDomain(tt.fqdn)
		if tt.wantErr {
			require.Error(t, err)
		} else {
			require.Equal(t, tt.want, got)
		}
	}
}

module gitlab.oit.duke.edu/devil-ops/certbot-locksmith

go 1.18

require (
	github.com/apex/log v1.9.0
	github.com/stretchr/testify v1.6.1
	gitlab.oit.duke.edu/devil-ops/aka-sdk v0.3.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pkg/errors v0.8.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200605160147-a5ece683394c // indirect
)

package main

import (
	"context"
	"os"
	"time"

	"github.com/apex/log"
	"gitlab.oit.duke.edu/devil-ops/aka-sdk/go/aka"
	"gitlab.oit.duke.edu/devil-ops/certbot-locksmith/certbot"
)

const (
	dnsWaitSec = 25
)

func main() {
	debug := os.Getenv("CERTBOT_DEBUG")
	if debug != "" {
		log.SetLevel(log.DebugLevel)
	}
	// Make sure the correct env vars are being passed in
	err := certbot.ValidateAuthEnv()
	certbot.CheckErr(err, "")
	certbotDomain := os.Getenv("CERTBOT_DOMAIN")
	// certbotDomain, err := certbot.ExtractDomain(certbotDomainE)
	// certbot.CheckErr(err, "Could not extract domain from certbot domain")
	certbotValidation := os.Getenv("CERTBOT_VALIDATION")

	// Create a new aka API client
	akaClient, err := aka.NewClientWithEnv()
	certbot.CheckErr(err, "Could not create AKA client")

	// Even though we are only using CERTBOT_DOMAIN now, do a loop for when
	// we use CERTBOT_ALL_DOMAINS maybe?
	ctx := context.Background()
	domains := []string{certbotDomain}
	for _, domain := range domains {
		createDomain := certbot.CreateChallengeDomainFromDomain(domain)

		// Does the domain exist?
		res, err := akaClient.GetDNSRecord(ctx, createDomain)
		certbot.CheckErr(err, "Could not check if DNS record exists")
		if len(res) > 0 {
			log.WithFields(log.Fields{
				"domain":       domain,
				"createDomain": createDomain,
			}).Fatal("Domain already exists")
		}

		log.WithFields(log.Fields{
			"domain":       domain,
			"createDomain": createDomain,
		}).Debug("Attemping to register domain in AKA")
		params := &aka.DNSParams{
			Name:  createDomain,
			Type:  "TXT",
			TTL:   30,
			RData: certbotValidation,
		}
		_, err = akaClient.CreateDNSRecord(ctx, *params)
		certbot.CheckErr(err, "Could not create DNS TXT record. Does it already exist? You may need to delete it manually.")

		log.WithFields(log.Fields{
			"domain":       domain,
			"createDomain": createDomain,
			"sleep":        dnsWaitSec,
		}).Debug("Created DNS TXT record, sleeping for propogation 👍")
		time.Sleep(dnsWaitSec * time.Second)

	}
	log.Debug("❤️ Thanks for using ACME! This keeps Duke safe and hopefully means less work for you in the long run!")
}
